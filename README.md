# Operation

Top pin descriptions:

* 1x2 (right angle): Battery disconnect
* 1x2: DC input (5-9V)
* 1x3: Serial communications header
* 2x3: ICSP programming header

The steel plates and the container are connected to ground.

## Power

The battery voltage can be tested by placing a multimeter between one of the
right angled pins and ground (eg the container). The battery is fully charged
at around 4.2V and should last >10 hours even with the laser on.

To charge the battery, place the jumper across the battery disconnect pins and
connect a DC power supply to the DC input pins.

## Communications

The sensor can be communicated with over serial. To do so, connect a USB serial
adapter (eg FTDI232) to the serial header. The sensor serial operates with a
Baud rate of 460800 Bd. Connect to it using a terminal application, such as
PuTTY or Realterm on Windows or GNU Screen on Linux.

* PuTTY:
    1. Find the COM port of the USB serial device.
        1. Open device manager.
        2. Navigate to the "Ports (COM & LPT)" section and find your USB
            Serial Port with should be labelled with a COM number (eg COM1).
        If it is not there, you may need to look up instructions specific to
        your USB serial device.
    2. Open PuTTY.
    3. Select the Serial Connection type.
    4. Type the COM port into the Serial line field (eg COM1).
    5. Type the Baud rate of 460800 into the Speed field.
    6. Press Open.
* GNU Screen:
    1. Plug in the USB serial adapter and check dmesg for its device name (eg
        /dev/ttyACM0 or /dev/ttyUSB0)
    2. Run screen with the device name and the Baud rate:
        `$ screen -L /dev/ttyACM0 460800`. The -L option causes screen to keep a
        log of the text that is received (which is useful for recording the
        measurement report).

The connection can be tested by pressing enter when connected. If "Unknown
command:" is printed after every press of enter, the connection is good.

## Interface

The sensor is controlled via a text command interface. Commands are sent via the
serial connection established in the previous section and the response will be
output back over the same connection. Some of the available commands are:

1. `help`: prints out a list of available commands.
2. `status`: prints out the current configuration.
    * `TEXT_BUFFER_SIZE`: the maximum length (in characters) of a command line.
    * `echo_characters`: whether the input is printed to output or not.
    * `current FCY`: the frequency of microcontroller.
    * `sample_period_us`: the time between sensor measurements in microseconds.
    * `period_register_count`: ?
    * `number_of_signals`: the number of sensors that are being sampled (should
        always be 2).
    * `bytes_per_sample_set`: the number of bytes per sample set.
    * `storage`: the location where the samples are stored (INT=internal
        (faster, smaller), EXT=external (slower, bigger))
    * `total_samples`: the number of samples that can be stored in memory.
    * `record_period_in_seconds`: the length of time that can be recorded in
        seconds.
    * `sample_id`: the index of the oldest sample.
    * `trigger_source`: how the sensor is triggered (INT=sensor input,
        EXT=interrupt pin (inaccessible))
    * `post_trigger_samples`: the number of samples recorded after the trigger
        event.
    * `post_trigger_percent`: the percent of samples that are recorded after the
        trigger event.
    * `trigger_signal`: the channel used as the trigger source (should be 0
        (accelerometer)).
    * `trigger_slope`: POS=trigger when signal goes above level, NEG=trigger
        when signal goes below level.
    * `trigger_level`: input value to trigger at.
    * `trigger_hold_us`: trigger only after signal stays above/below level for
        this length of time in microseconds (useful to prevent triggering by
        impacts).
    * `trigger_hold_samples`: same as `trigger_hold_us` but in samples not
        microseconds.
3. `sample`: enters sampling mode. This state will only be exited after the
    sampling has been triggered or the sensor is powered off and on again (by
    removing and replacing the battery disconnect jumper).
4. `report`: prints out the contents or the sensor's memory in CSV form. The
    first column is the sample number (which can be converted to time by
    multiplying by the sample period (`sample_period_us` in microseconds from
    the `status` command)), the second column is the accelerometer measurements
    and the third column is the photodiode (checkpoint) measurements.

## Example operation

1. Make markings inside compression tube.
2. Make sure the sensor is charged to about 4.2V.
3. Connect to the sensor via serial.
4. Configure the trigger and other settings.
5. Use the command `sample` to begin sampling.
6. Seal container.
7. Run test.
8. Open container.
9. Connect to the sensor via serial.
10. Use the command `report` to print out the data.
11. Save the data into a CSV file and process it using x3process.py (see
    x3process/README.md).
