#!/usr/bin/env python

import sys
import numpy
from scipy.optimize import minimize

def apply_calibration(calibration, data_accelerometer):
    # Subtract zero, apply sensitivity and return output
    return (data_accelerometer - calibration[0]) * calibration[1]

def integrate(data):
    # Make arrays for velocity and position
    # Initially both are 0 at time = 0
    velocity = numpy.zeros((data.shape[0],))
    position = numpy.zeros((data.shape[0],))

    # Integrate
    for n in range(data.shape[0] - 1):
        # Change in time
        dt = data[n + 1, 0] - data[n, 0]

        # Increase velocity with acceleration trapezoid
        velocity[n + 1] = velocity[n] + \
            (data[n, 1] + data[n + 1, 1]) * (dt / 2)

        # Increase position with velocity trapezoid
        position[n + 1] = position[n] + \
            (velocity[n] + velocity[n + 1]) * (dt / 2)

    # Return velocity and position
    return velocity, position

def cost(calibration, data, checkpoints):
    # Get calibrated acceleration
    acceleration = apply_calibration(calibration, data[:, 1])

    # Get the velocity and position
    velocity, position = integrate(
        numpy.stack([data[:, 0], acceleration], axis=1))

    # Interpolate position at the checkpoint times
    position_at_checkpoints = numpy.interp(checkpoints[:, 0],
        data[:, 0], position)

    # Return the difference between these
    return numpy.square(position_at_checkpoints - checkpoints[:, 1]).mean()

def get_calibration(state, data, checkpoints):
    # Add initial position as a checkpoint
    # zero_checkpoint_time = numpy.arange(0,
    #     state["zero_region_end"],
    #     state["zero_region_end"] / (checkpoints.shape[0] * 2))
    zero_checkpoint_time = numpy.array([state["zero_region_end"]],
        numpy.float64)
    zero_checkpoint_position = numpy.zeros(zero_checkpoint_time.shape)
    zero_checkpoints = numpy.stack(
        [zero_checkpoint_time, zero_checkpoint_position], axis=1)
    print(zero_checkpoints.shape)
    print(checkpoints.shape)
    checkpoints = numpy.concatenate([zero_checkpoints, checkpoints], axis=0)
    print(checkpoints)
    zero_guess = data[data[:, 0] < state["zero_region_end"], 1].mean()
    print("zero_guess", zero_guess)

    # Minimisation:
    #  Parameters: calibration = [zero, sensitivity]
    #   zero in ADC levels
    #   sensitivity in (m/s/s)/ADC
    #  Cost: total difference to checkpoints

    # Range
    calibration_bounds = numpy.array([
            [zero_guess - 10, zero_guess + 10],
            [0, 30]
        ], numpy.float64)

    # Make initial guess
    initial_calibration = numpy.array([
            zero_guess,
            1
        ], numpy.float64)

    # Function to minimise
    minimise_me = lambda calibration: cost(calibration, data, checkpoints)

    # Do minimisation
    minimise_result = minimize(minimise_me, initial_calibration,
        bounds=calibration_bounds,
        options={"disp": True},
        callback=lambda xk: print(xk))
    state["calibration"] = minimise_result["x"]

    # Return new state
    return state
