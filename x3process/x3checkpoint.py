#!/usr/bin/env python

import matplotlib.pyplot as pyplot
import numpy
import pickle
import sys
import time

# matplotlib click event
clicks = []
def onclick(event):
    clicks.append(event)

def get_checkpoints(state, data_time, data_accelerometer, data_laser):
    # Show acceleration data
    figure = pyplot.figure()
    axes = figure.add_subplot(111)
    axes.set_title("Accelerometer measurement")
    axes.set_xlabel("Time (s)")
    axes.set_ylabel("Accelerometer (ADC)")
    axes.plot(data_time, data_accelerometer)
    cid = figure.canvas.mpl_connect('button_press_event', onclick)
    pyplot.show(block=False)

    # Ask for end of zero region
    axes.set_xlabel(
        "Time (s)\nClick just before the acceleration kicks in",
        labelpad=0.5)
    while len(clicks) == 0:
        pyplot.pause(0.1)
    click = clicks.pop()
    state["zero_region_end"] = click.xdata

    # Disconnect button_press_event
    figure.canvas.mpl_disconnect(cid)

    # Get zero region
    zero_region = data_time > state["zero_region_end"]

    # Show laser data
    pyplot.close()
    figure = pyplot.figure()
    axes = figure.add_subplot(111)
    axes.set_title("Laser reflection intensity")
    axes.set_xlabel("Time (s)")
    axes.set_ylabel("Reflection intensity")
    axes.set_yticks([])
    axes.plot(data_time[zero_region], data_laser[zero_region])
    cid = figure.canvas.mpl_connect('button_press_event', onclick)
    pyplot.show(block=False)

    # Ask for checkpoint region
    points = ["start", "end"]
    for p in range(2):
        axes.set_xlabel(
            "Time (s)\nClick on checkpoint region %s" % points[p],
            labelpad=0.5)
        while len(clicks) == 0:
            pyplot.pause(0.1)
        click = clicks.pop()
        state["checkpoint_region"][p] = click.xdata
        axes.plot(
            numpy.array([1, 1], numpy.float64) * state["checkpoint_region"][p],
            numpy.array([data_laser.min(),
                         data_laser.max()], numpy.float64))
        figure.canvas.draw_idle()

    # Disconnect button_press_event
    figure.canvas.mpl_disconnect(cid)

    # Zoom to checkpoint region
    print("Zooming to [%f, %f]" % (state["checkpoint_region"][0],
        state["checkpoint_region"][1]))
    checkpoint_region = numpy.logical_and(
        data_time >= state["checkpoint_region"][0],
        data_time <= state["checkpoint_region"][1])
    pyplot.close()
    figure = pyplot.figure()
    axes = figure.add_subplot(111)
    axes.set_title("Laser reflection intensity")
    axes.set_xlabel("Time (s)")
    axes.set_ylabel("Reflection intensity")
    axes.set_yticks([])
    axes.plot(data_time[checkpoint_region], data_laser[checkpoint_region])
    cid = figure.canvas.mpl_connect('button_press_event', onclick)
    pyplot.show(block=False)

    # Ask for checkpoint times
    for c in range(4):
        axes.set_xlabel(
            "Time (s)\nClick on checkpoint %d" % (c + 1),
            labelpad=0.5)
        while len(clicks) == 0:
            pyplot.pause(0.1)
        click = clicks.pop()
        state["checkpoints"][c, 0] = click.xdata
        pyplot.plot(
            numpy.array([1, 1], numpy.float64) * state["checkpoints"][c, 0],
            numpy.array([data_laser[checkpoint_region].min(),
                         data_laser[checkpoint_region].max()], numpy.float64))
        pyplot.draw()

    # Disconnect button_press_event
    figure.canvas.mpl_disconnect(cid)

    # Ask for checkpoint positions
    axes.set_xlabel(
        "Time (s)\nEnter checkpoint positions in terminal",
        labelpad=0.5)
    print("Enter checkpoint positions or leave blank for previous:")
    for c in range(4):
        checkpoint_position = input(
            "checkpoint %d [%f]: " % (c + 1, state["checkpoints"][c, 1]))
        if len(checkpoint_position) > 0:
            state["checkpoints"][c, 1] = float(checkpoint_position)

    # Close figure
    pyplot.close(figure)

    # Return new state
    return state
