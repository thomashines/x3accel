# Scripts to reconstruct trajectory from sensor measurements.

Use the x3process.py script with the data reported by the sensor package.

An example dataset is in example_input.csv. To process this data:

1. Run x3process.py with `$ ./x3process.py example_input.csv`
2. Type y then hit enter to enter new checkpoints.
3. Click on the acceleration plot just before the piston begins to accelerate.
    In this case, click the plot at about 0.24 seconds.
4. Click before then after the region of the reflection intensity plot where the
    piston passes by the checkpoints. In this case click at about 0.7 then 0.75
    seconds.
5. Click on the 4 troughs in the reflection signal that relate to the markings
    of the checkpoints in the tube from left to right. In this case click at
    about 0.7250, 0.7285, 0.7315 and 0.7350 seconds.
6. Enter the positions of the checkpoints. In this case enter 0.75, 0.755, 0.76
    and 0.765.
7. Type y then hit enter to calculate the calibration factors. This step may
    take some time.
8. A plot should be displayed that shows the calibrated trajectory. Also, the
    calibration factors (accelerometer zero and accelerometer sensitivity) along
    with some other information is printed to the terminal. The calibrated data
    is also written to 3 csv files:
    * calibrated.csv: contains the calibrated measurements as well as the
        derived velocity and position estimates.
    * calibration.csv: contains the calibration factors.
    * checkpoints.csv: contains the positions and times of the checkpoints.
