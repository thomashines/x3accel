#!/usr/bin/env python

import matplotlib.pyplot as pyplot
import numpy
import os
import pickle
import sys
import time

from x3calibrate import apply_calibration, get_calibration, integrate
from x3checkpoint import get_checkpoints

# eDAQS config
sample_period = 35E-6

# matplotlib click event
clicks = []
def onclick(event):
    clicks.append(event)

if __name__ == "__main__":
    # Input:
    #  Measurements: time, accelerometer, laser
    #  Previous state: checkpoint region, checkpoint times
    # Controls:
    #  Checkpoints: checkpoint region, checkpoint times
    # Output:
    #  Calibrated data: time, acceleration, velocity, position

    if len(sys.argv) != 2:
        print("Usage: ./x3process.py <data.csv>")
        sys.exit(1)

    # Load data
    data = None
    with open(sys.argv[1], "rb") as data_file:
        data = numpy.loadtxt(data_file, delimiter=",")
    data_time = data[:,0] * sample_period
    data_accelerometer = data[:,1]
    data_laser = data[:,2]

    # Check for previous state
    state = {}
    if os.path.isfile("state.data"):
        # Load previous state
        with open("state.data", "rb") as state_file:
            state = pickle.load(state_file)

    # Check state completeness, fill with mock data
    state["zero_region_end"] = state.get("zero_region_end", 0.1)
    state["checkpoint_region"] = state.get("checkpoint_region",
        numpy.array([0.1, 0.2], numpy.float64))
    state["checkpoints"] = state.get("checkpoints",
        numpy.array([
                [0.11, 0.71],
                [0.12, 0.72],
                [0.13, 0.73],
                [0.14, 0.74],
            ], numpy.float64))
    state["calibration"] = state.get("calibration",
        numpy.array([2048, 0.5], numpy.float64))

    # Save state
    with open("state.data", "wb") as state_file:
        pickle.dump(state, state_file)

    # Get checkpoints
    y = input("Enter new checkpoints? [yN]: ")
    if y == "y":
        state = get_checkpoints(state, data_time, data_accelerometer,
            data_laser)

    # Save checkpoints
    with open("checkpoints.csv", "wb") as checkpoints_file:
        numpy.savetxt(checkpoints_file, state["checkpoints"],
            header="time,position", delimiter=",")

    # Do calibration
    y = input("Calculate new calibration? [yN]: ")
    if y == "y":
        state = get_calibration(state,
            numpy.stack([data_time, data_accelerometer], axis=1),
            state["checkpoints"])
    print("accelerometer zero", state["calibration"][0])
    print("accelerometer sensitivity", state["calibration"][1], "(m/s/s)/ADC")

    # Save calibration
    with open("calibration.csv", "wb") as calibration_file:
        numpy.savetxt(calibration_file, state["calibration"].reshape((1, 2)),
            header="zero,sensitivity", delimiter=",")

    # Get calibrated data
    calibration_multiplier = numpy.array([
        1.0000,
        1.0000
    ])
    acceleration = apply_calibration(
        state["calibration"] * calibration_multiplier,
        data_accelerometer)
    velocity, position = integrate(
        numpy.stack([data_time, acceleration], axis=1))

    # Get velocity at checkpoints
    velocity_at_checkpoints = numpy.interp(state["checkpoints"][:, 0],
        data_time, velocity)
    print("velocity_at_checkpoints", velocity_at_checkpoints)
    sample_period = data_time[1:] - data_time[:-1]
    sample_rate = 1/sample_period.mean()
    print("sample_rate", sample_rate)

    # Stop at the end
    test_end_region = numpy.logical_and(
        data_time > state["checkpoints"][:, 0].max(),
        velocity > 0)
    test_end = data_time[test_end_region].max()
    test_region = data_time <= test_end
    data_time = data_time[test_region]
    acceleration = acceleration[test_region]
    velocity = velocity[test_region]
    position = position[test_region]
    data_laser = data_laser[test_region]

    # Get extremes
    print("acceleration", acceleration.min() / 9.81, acceleration.max() / 9.81)
    print("velocity", velocity.min(), velocity.max())

    # Smooth acceleration for plotting
    n = 100
    acceleration_smoothed = numpy.cumsum(acceleration)
    acceleration_smoothed[n:] = acceleration_smoothed[n:] - \
        acceleration_smoothed[:-n]
    acceleration_smoothed = acceleration_smoothed / n

    # Save calibrated data
    with open("calibrated.csv", "wb") as calibrated_file:
        numpy.savetxt(calibrated_file,
            numpy.stack([data_time,
                         acceleration,
                         velocity,
                         position,
                         data_laser,
                         acceleration_smoothed], axis=1),
            header="time,acceleration,velocity,position,reflectance," +
                "smoothed_acceleration",
            delimiter=",")

    # Save state
    with open("state.data", "wb") as state_file:
        pickle.dump(state, state_file)

    # Plot position
    pyplot.plot(data_time, position)
    pyplot.plot(state["checkpoints"][:, 0], state["checkpoints"][:, 1], "ro")
    pyplot.show()
