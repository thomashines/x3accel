# Programming


* ICD3 (RJ-11, left to right)
 * 1 - PGM (NC)
 * 2 - PGC
 * 3 - PGD
 * 4 - GND
 * 5 - VDD
 * 6 - !MCLR/VPP


* DAQS
 *  3V3, SDA,  NC
 * MCLR, GND, CLK
 * PGED2 - SDA
 * PGEC2 - SCL


* ICD3 -> DAQS
 *   PGC -> CLK
 *   PGD -> SDA
 *   GND -> GND
 *   VDD -> 3V3
 * !MCLR -> !MCLR
