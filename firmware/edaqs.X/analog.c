/*
 * File:   analog.c
 * Author: peterj
 *
 * Created on 22 February 2009.
 * Set up and read the ADC channels on DRB's DAQS board.
 * Adapted from PIC24 experiments.
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
# else
#  error "not a valid MCU selection"
# endif
#endif

// eDAQS modules
#include "analog.h"
#include "osc.h"

void initADC(void) {
    AD1CON1bits.ADON = 0; // turn off for config
    // 0. 12-bit converter
    AD1CON1bits.AD12B = 1;
    // 1. Specify port pins as analog input
    //    (was done in initHardware())
    // 2. Select voltage references
    //    AVss and AVdd are used as Vref- and Vref+ respectively
    AD1CON2bits.VCFG = 0b000;
    // 3. Conversion clock
    AD1CON3bits.ADRC = 0; // clock derived from system clock
    // AD1CON3bits.ADRC = 1; // use internal RC clock
    AD1CON3bits.ADCS = 1; // Tad = 2 * Tcy = 135ns (> 118ns minumum)
    AD1CON3bits.SAMC = 3; // Tsample, >=3 Tad (minimum)
    // 4. Sample and hold channel
    // We will be later changing CH0SA when selecting signals.
    AD1CON2bits.CHPS = 0b00; // converts CH0 only
    AD1CHS0bits.CH0NA = 0; // Channel 0 negative input, sample A
    AD1CHS0bits.CH0SA = 0; // Channel 0 positive input
    AD1CHS0bits.CH0NB = 0; // Channel 0 negative input, sample B
    AD1CHS0bits.CH0SB = 0; // Channel 0 positive input
    AD1CON2bits.ALTS = 0; // always use inputs for sample A
    // 5. Appropriate sample/conversion sequence.
    AD1CON1bits.SSRC = 0b111; // automatic conversion to start after sampling
    AD1CON1bits.ASAM = 0; // sampling starts when SAMP bit set
    AD1CSSL = 0; // no scanning required
    // 6. Presentation of conversion results
    AD1CON1bits.FORM = 0b00; // present as unsigned int
    // 7. Turn on the ADC module.
    AD1CON1bits.ADON = 1;
    return;
}

// Select and sample a particular analog pin.
unsigned int readADC( unsigned char which_ANx ) {
    // Clear DONE bit, just in case it is left on
    // from a previous conversion.
    // If we don't do this, we may drop straight through the
    // waiting (while) loop and pick up an old result.
    AD1CON1bits.DONE = 0;

    // Select the channel
    AD1CHS0bits.CH0SA = which_ANx; // Channel 0 positive input
    // AD1CHS0bits.CH0SB = which_ANx; // Channel 0 positive input

    AD1CON1bits.SAMP = 1; // start sampling
    while ( !AD1CON1bits.DONE ) ; // wait for conversion
    return ADC1BUF0;
}
