/*
 * File:   my_timer.c
 * Author: peterj
 *
 * Created on 23 February 2009.
 * From daqs-drb project. We'll use timer 2 to provide a regular tick.
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
# else
#  error "not a valid MCU selection"
# endif
#endif

// eDAQS modules
#include "my_timer.h"
#include "osc.h"

unsigned int compute_period_register_count(unsigned int period_us) {
    // The following arrangement is an attempt to avoid overflow
    // of the intermediate result and premature truncation.
    // We also assume. PRE=8.
    return (int)(((long)period_us * (getFCY()/800L))/10000L) - 1;
}

// Get Timer2 ticking over as a 16-bit timer.
// Period is in microseconds.
void initTimer2(unsigned int period_us) {
    T2CONbits.T32 = 0;  // 16-bit
    T2CONbits.TCS = 0;  // MCU clock as source
    T2CONbits.TGATE = 0;
    T2CONbits.TCKPS = 0b01; // prescale 8
    TMR2 = 0;
    // Clear the interrupt flag.
    // Even though we are not using interrupts, this flag
    // is set by the hardware when TMR2 reaches PR2.
    IFS0bits.T2IF = 0;
    PR2  = compute_period_register_count(period_us);
    T2CONbits.TON = 1; // enable timer
    return;
}

void startTimer2() {
    TMR2 = 0;
    IFS0bits.T2IF = 0;
    T2CONbits.TON = 1;
    return;
}

void stopTimer2() {
    T2CONbits.TON = 0;
    TMR2 = 0;
    IFS0bits.T2IF = 0;
    return;
}

void waitTimer2() {
    while ( !IFS0bits.T2IF ) /* wait */ ;
    IFS0bits.T2IF = 0; // reset the interrupt flag
    return;
}
