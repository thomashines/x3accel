/*
 * File:   my_delay.h
 * Author: peterj
 *
 * Created on ???.
 * Simple software delay loop.
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
# else
#  error "not a valid MCU selection"
# endif
#endif

// eDAQS modules
#include "my_delay.h"

int my_delay(int count) {
    unsigned int i, j, k;
    k = 0;
    for ( i = 0; i < count; ++i ) {
        k = 0;
        for ( j = 0; j < 0xff; ++j ) {
            k += 1;
        }
    }
    return k;
}
