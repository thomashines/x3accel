/* Microchip Technology Inc. and its subsidiaries.  You may use this software
 * and any derivatives exclusively with Microchip products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
 * TERMS.
 */

/*
 * File:   my_uart.h
 * Author: peterj
 *
 * Created on 20 February 2009.
 * Functions adapted from the Microchip datasheet information.
 */

// This is a guard condition so that contents of this file are not included
// more than once.
#ifndef MY_UART_H
#define	MY_UART_H

#include <xc.h> // include processor files - each processor file is guarded.

// #include <>

// C++ class definitions if appropriate

// Declarations or function prototypes

// A value of 115200L works everywhere, but is oh-so-slow.
// We have successfully run at 921600L with minicom, however,
// Tcl/Tk did not successfully work at that speed.
// Looking into the source code of tclUnixChan, only baud rates
// up to 460800 are listed; let's go with that.
#define DEFAULT_BAUD_RATE 460800L

// Various special characters that we might encounter.
#define ACK 0x01
#define BS 0x7F
#define CR 0x0D
#define LF 0x0A
#define EOT 0x04
#define ETX 0x03

void initUART1(unsigned long baud_rate);
char putcU1(char c);
char getcU1(void);
unsigned int putsU1(char *s);
char * getsnU1(char *buf, unsigned int n, unsigned char with_echo);
int write(int handle, void *buffer, unsigned int len);
void printResetCause(void);

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C
    // linkage so the functions can be used by the c code.

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* MY_UART_H */
