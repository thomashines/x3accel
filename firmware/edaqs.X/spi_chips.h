/* Microchip Technology Inc. and its subsidiaries.  You may use this software
 * and any derivatives exclusively with Microchip products.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
 * TERMS.
 */

/*
 * File:   spi_chips.h
 * Author: peterj
 *
 * Created on 1 February 2014.
 * From daqs-drb project.
 */

// This is a guard condition so that contents of this file are not included
// more than once.
#ifndef SPI_CHIPS_H
#define	SPI_CHIPS_H

#include <xc.h> // include processor files - each processor file is guarded.

// #include <>

// C++ class definitions if appropriate

// Declarations or function prototypes

// Mode register values for 23LC1024
#define SRAM_BYTE_MODE 0b00000000
#define SRAM_PAGE_MODE 0b10000000
#define SRAM_SEQUENTIAL_MODE 0b01000000

enum spi_chips_t {SPI_RAM, SPI_DP};

void selectSPIchip(enum spi_chips_t whichChip);
void releaseSPIchip(enum spi_chips_t whichChip);
void initSPIx(void);
void putcSPIx(unsigned char cout);
unsigned char setSRAMmode(enum spi_chips_t which_chip, unsigned char mode);
unsigned char writeSRAMdata(enum spi_chips_t which_chip, unsigned long addr,
                            unsigned char *bufout, unsigned int n);
unsigned char writeSRAMdataFast(unsigned long addr,
                                unsigned char *bufout, unsigned int n);
unsigned char readSRAMdata(enum spi_chips_t which_chip, unsigned long addr,
                           unsigned char *bufin, unsigned int n);
unsigned char enableDP(void);
unsigned char writeDP(char which_dp, char res);

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C
    // linkage so the functions can be used by the c code.

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* SPI_CHIPS_H */
