/*
 * File:   edaqs_master.c
 * Author: peterj
 *
 * Created on 29 January 2014.
 * Start of the command-line interpreter (mostly ported from daqs-drb).
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
# else
#  error "not a valid MCU selection"
# endif
#endif

// Standard includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Set the relevant bits in the configuration registers.
// Start up using FRC oscillator
_FOSCSEL(FNOSC_FRC)
// Enable clock switching; we have a 7.3728MHz crystal, medium speed.
_FOSC(FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_XT)
// No watchdog timer for the moment.
_FWDT(FWDTEN_OFF)
// Disable JTAG and have debug pins at PGED2,PGEC2
_FICD(JTAGEN_OFF & ICS_PGD2)

// eDAQS modules
#include "analog.h"
#include "my_delay.h"
#include "my_timer.h"
#include "my_uart.h"
#include "osc.h"
#include "spi_chips.h"

// Macros --------------------------------------------------------------

#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
        _a > _b ? _a : _b; })

#define min(a,b) \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
        _a < _b ? _a : _b; })

// Variables and constants ---------------------------------------------

#define VERSION_STRING "0.04 20-Aug-2016"
#define PROMPT_STRING "\r\nok> "
// A buffer to contain incoming text commands.
#define TEXT_BUFFER_SIZE 32
static char text_buffer[TEXT_BUFFER_SIZE];
// Flag to indicate that the getCommandString function should echo characters
static char echo_characters = 1;
// Sample period in microseconds
static unsigned int sample_period_us = 100;

// The data_blob is an array of bytes on the MCU for accumulating
// our recorded data.  We can store data quickly to this blob.
// There is also an external 23LC1024 SRAM chip sitting off the SPI port.
// It is available is for the slower but longer records -- the default.
#ifdef __dsPIC33EP256GP502__
// Keep array less than 32kB
# define MAX_SAMPLES_ON_MCU 2048
#endif
#ifdef __dsPIC33FJ128GP802__
// Keep array less than 8kB, but don't really know if this is the limit.
# define MAX_SAMPLES_ON_MCU 512
#endif
#define MAX_ANALOG_SIGNALS 6
static unsigned int data_blob[MAX_ANALOG_SIGNALS][MAX_SAMPLES_ON_MCU];
#define BYTES_IN_SRAM_CHIP 131072L
// we pack signal data into sample sets of 16-bytes
// This allows room for digital data at a later date.
enum memory_options {RAM_INT, RAM_EXT};
static enum memory_options storage = RAM_EXT;

// Mapping from analog signal number to ANx index
// Will be used to select the signal to be routed to the sample-hold amp.
// The reason for this mapping is that, in order to get 10 channels
// on a dsPIC33, we need to go to higher analog inputs, beyong An7 etc.
unsigned char anx[] = {0, 1, 2, 3, 4, 5};
// We may want to (later) reduce the number of scanned signals.
static unsigned int number_of_signals = MAX_ANALOG_SIGNALS;

// We're going to make some assumptions here, that we'll be using
// the full array of signals and packing two 12-bit values into 3 bytes.
#define NOMINAL_BYTES_PER_SAMPLE_SET ((MAX_ANALOG_SIGNALS * 3) / 2)
static unsigned char bytes_per_sample_set = NOMINAL_BYTES_PER_SAMPLE_SET;
#define MAX_SAMPLE_SETS (BYTES_IN_SRAM_CHIP / NOMINAL_BYTES_PER_SAMPLE_SET)
static unsigned long total_samples = MAX_SAMPLE_SETS;
static unsigned long sample_id = 0; // identifies the oldest sample
    // which is also the next sample to be written over when recording
static unsigned long post_trigger_samples = MAX_SAMPLE_SETS / 2;

// Trigger event settings
enum trigger_options {TRIG_INT, TRIG_EXT};
static enum trigger_options trigger_source = TRIG_INT;
static unsigned char trigger_signal = 0; // trigger source
enum slope_options {POS, NEG};
static enum slope_options trigger_slope = POS;
#define MAX_TRIGGER_LEVEL 4095
static unsigned int trigger_level = MAX_TRIGGER_LEVEL / 2; // half-way
static unsigned int trigger_hold_us = 1; // time trigger must be held for
// samples trigger must be held for
static unsigned int trigger_hold_samples = 1000;
#define DEFAULT_POST_TRIGGER_PERCENT 90
static unsigned long post_trigger_percent = DEFAULT_POST_TRIGGER_PERCENT;

// Digipot settigns
static unsigned int digipot_settings[2];

// Buffers for use with the external SPI SRAM chips.
static unsigned char pageBuf0[32];
static unsigned char pageBuf1[32];

// We'll use the pin to flag certain events such as the start
// and stop of analog conversion, writing to SPI-RAM etc.
#define LED_PIN LATAbits.LATA4

#define EVENT_PIN PORTBbits.RB14

// X3 piston accelerometer laser
// Laser is active high, connected to CS_B (pin RB4)
#define LASER (LATBbits.LATB4)

// ---------------------------------------------------------------------

// Initialize the MCU peripheral hardware.
void initHardware(void) {
    RCONbits.SWDTEN = 0;  // Disable the watchdog timer.
    TRISAbits.TRISA4 = 0; // LED is on RA4. Set to output
    LATAbits.LATA4 = 0;   // Initially LED is off.
    TRISBbits.TRISB14 = 1; // !event pin defaults to input
    CNPUBbits.CNPUB14 = 1; // enable weak pull-up here
#   ifdef __dsPIC33EP256GP502__
      // Analog input
      ANSELAbits.ANSA0 = 1;
      ANSELAbits.ANSA1 = 1;
      ANSELBbits.ANSB0 = 1;
      ANSELBbits.ANSB1 = 1;
      ANSELBbits.ANSB2 = 1;
      ANSELBbits.ANSB3 = 1;
      // Not analog.
      ANSELAbits.ANSA4 = 0;
      ANSELBbits.ANSB8 = 0;
#   endif
#   ifdef __dsPIC33FJ128GP802__
      // Analog input
      AD1PCFGLbits.PCFG0 = 0;
      AD1PCFGLbits.PCFG1 = 0;
      AD1PCFGLbits.PCFG2 = 0;
      AD1PCFGLbits.PCFG3 = 0;
      AD1PCFGLbits.PCFG4 = 0;
      AD1PCFGLbits.PCFG5 = 0;
      // Not analog input; we use these pins for SPI
      AD1PCFGLbits.PCFG9 = 1;
      AD1PCFGLbits.PCFG10 = 1;
      AD1PCFGLbits.PCFG11 = 1;
      AD1PCFGLbits.PCFG12 = 1;
#   endif
    TRISBbits.TRISB10 = 0; // SDOx output
    TRISBbits.TRISB11 = 0; // SCKx output
    TRISBbits.TRISB12 = 1; // SDIx input
    TRISBbits.TRISB13 = 0; // chip-select-SRAM output
    LATBbits.LATB13 = 1;
    TRISBbits.TRISB7 = 0;  // chip-select-AFE output
    LATBbits.LATB7 = 1;

    // Laser
    TRISBbits.TRISB4 = 0; // Laser output
    LATBbits.LATB4 = 0; // Laser off

    //
    // Determine the function of some remappable pins
    // First, unlock Peripheral Pin Select
    OSCCON = 0x0046;
    OSCCON = 0x0057;
    OSCCONbits.IOLOCK = 0;
    // Map pins depending on which MCU
#   ifdef __dsPIC33EP256GP502__
      RPINR18bits.U1RXR = 41;      // U1RX <-- RP41(RB9)
      RPOR3bits.RP40R = 0b000001;  // RP40(RB8) <-- U1TX
      RPINR22bits.SDI2R = 44;      // SDI2 <-- RPI44(RB12)
      RPOR4bits.RP42R = 0b001000;  // SDO2 --> RPI42(RB10)
      RPOR4bits.RP43R = 0b001001;  // SCK2 --> RPI43(RB11)
#   else
#     ifdef __dsPIC33FJ128GP802__
        RPINR18bits.U1RXR = 9;      // U1RX <-- RP9(RB9)
        RPOR4bits.RP8R = 0b00011;   // RP8(RB8) <-- U1TX
        RPINR20bits.SDI1R = 12;     // SDI1 <-- RP12(RB12)
        RPOR5bits.RP10R = 0b00111;  // SDO1 --> RP10(RB10)
        RPOR5bits.RP11R = 0b01000;  // SCK1 --> RP11(RB11)
#     else
#       error "not a valid MCU selection for this project"
#     endif
#   endif
    // Finally, lock PPS
    OSCCON = 0x0046;
    OSCCON = 0x0057;
    OSCCONbits.IOLOCK = 1;

    return;
}

// ---------------------------------------------------------------------

// Start up the timer and start sampling,
// writing sample sets to the SRAM chips.
void doSampling() {
    unsigned char done = 0;
    unsigned int since_trigger = 0;
    unsigned char triggered = 0;
    unsigned long post_trigger_count = 0;
    unsigned int a0;
    unsigned int a1;
    unsigned long byte_address = 0; // in SRAM chip
    unsigned char *byte_ptr; // pointer to the first of three bytes of packed data

    // Start with laser off
    LASER = 0;

    // Timer for laser flash before trigger
    unsigned int flash_on = 4;
    unsigned int flash_timer = 0;

    initTimer2(sample_period_us);
    sample_id = 0; // start with a clean slate
    while ( !done ) {
        // Timing signal (to use this comment out the other uses of LASER and
        // get the timing from CSB (4th pin from the right))
        // LASER ^= 1;
        // 1. Take a set of samples.
        // Clear DONE bit, just in case it is left on from a previous conversion
        // A0: accelerometer
        AD1CON1bits.DONE = 0;
        AD1CHS0bits.CH0SA = 0;
        AD1CON1bits.SAMP = 1;
        while ( !AD1CON1bits.DONE ) ; // wait
        a0 = ADC1BUF0;
        // A1: photodiode
        AD1CON1bits.DONE = 0;
        AD1CHS0bits.CH0SA = 1;
        AD1CON1bits.SAMP = 1;
        while ( !AD1CON1bits.DONE ) ; // wait
        a1 = ADC1BUF0;
        // 2. Test for trigger event.
        // unsigned int trig_value = avalue[trigger_signal]; // a0
        if ((trigger_slope == POS && a0 > trigger_level) ||
                (trigger_slope == NEG && a0 < trigger_level)) {
            since_trigger++;
        } else {
            since_trigger = 0;
        }
        if (since_trigger >= trigger_hold_samples) {
            triggered = 1;
        }
        if (triggered) {
            post_trigger_count++;
        }
        if (triggered || since_trigger > 0) {
            // Set laser on
            LASER = 1;
        } else {
            // Flash laser
            flash_timer++;
            if (flash_timer < flash_on) {
                LASER = 1;
            } else {
                LASER = 0;
            }
        }
        // 3. Pack the 12-bit analogue data into 16-bytes.
        byte_ptr = pageBuf0;
        *byte_ptr++ = (char) a0; // bottom 8 bits
        // Second byte gets 4 bits from each value.
        *byte_ptr++ = (((char) (a0 >> 8)) & 0x0f) | ((char) (a1 << 4));
        *byte_ptr++ = (char) (a1 >> 4); // top 8 bits from original 12
        // 4. Send the sample-set to the SRAM chip.
        // 2014-02-02 Note that we send only just enough data bytes
        // presuming an even number of channels.
        byte_address = sample_id * bytes_per_sample_set;
        writeSRAMdataFast(byte_address, pageBuf0, bytes_per_sample_set);
        // Increment for the next sample set.
        // Eventually, we will be overwriting the oldest data.
        ++sample_id;
        if ( sample_id >= total_samples ) sample_id = 0;
        // 5. We finish the loop if we have collected the required
        //    number of post-trigger samples.
        if ( post_trigger_count >= post_trigger_samples ) done = 1;
        // 6. We sit and do nothing until the next period starts.
        waitTimer2();
    } // end while
    stopTimer2();

    // Turn off laser
    LASER = 0;

    return;
}

// ---------------------------------------------------------------------

void reportSamples(void) {
    unsigned long byte_address = 0; // in SRAM chip
    unsigned long next_sample;
    unsigned char *byte_ptr;
    unsigned char i, b0, b1, b2;
    unsigned int avalue[MAX_ANALOG_SIGNALS];
    unsigned int j;

    // 0. Header line to label columns.
    printf("\r\n\"Count\",");
    for ( i = 0; i < number_of_signals; ++i ) {
        printf("\"A%d\"", i);
        if ( i < number_of_signals-1 ) printf(",");
    }

    next_sample = sample_id; // identify the oldest sample set
    for ( j = 0; j < total_samples; ++j ) {
        if ( storage == RAM_EXT ) {
            // 1. Fetch the next sample set into the local buffer and unpack it.
            byte_address = next_sample * bytes_per_sample_set;
            readSRAMdata(SPI_RAM, byte_address, pageBuf0, bytes_per_sample_set);
            byte_ptr = pageBuf0;
            for ( i = 0; i < number_of_signals; i += 2 ) {
                b0 = *byte_ptr++;
                b1 = *byte_ptr++;
                b2 = *byte_ptr++;
                avalue[i] = ((((unsigned int) b1) << 8) & 0xf00) | b0;
                avalue[i+1] = ((((unsigned int) b2) << 4) & 0xff0) | ((b1 >> 4) & 0x00f);
            }
        } else {
            for ( i = 0; i < number_of_signals; i++ ) {
                avalue[i] = data_blob[i][next_sample];
            }
        }
        // 3. Report the values as ASCII text in comma-separated-value format.
        printf("\r\n%u,", j);
        for ( i = 0; i < number_of_signals; ++i ) {
            printf("%u", avalue[i]);
            if ( i < number_of_signals-1 ) printf(",");
        }
        // 4. Increment to the next sample set, wrapping around, if necessary.
        ++next_sample;
        if ( next_sample >= total_samples ) next_sample = 0;
    } // end for j

    return;
}

// ---------------------------------------------------------------------

unsigned int max_number_of_sample_sets(void) {
    if ( storage == RAM_INT ) {
        return MAX_SAMPLES_ON_MCU;
    } else {
        return BYTES_IN_SRAM_CHIP / bytes_per_sample_set;
    }
}

void set_derived_parameters(void) {
    bytes_per_sample_set = (number_of_signals * 3) / 2;
    total_samples = max_number_of_sample_sets();
    post_trigger_samples = (total_samples * post_trigger_percent) / 100;
    trigger_hold_samples = trigger_hold_us / sample_period_us;
    return;
}

double record_period_in_seconds(void) {
    return sample_period_us * total_samples * 1.0e-6;
}

void interpretCommandString(char *buf) {
    // Keep ivalue off the stack so we don't need extended pointers
    // for dsPIC33EP which may put the stack above 32kB.
    static unsigned char which_ANx;
    static unsigned int avalue, ivalue;

    if ( !strncmp(buf,"help",4) || !strncmp(buf,"?",1) ) {
        putsU1("\r\nAvailable commands are:");
        putsU1("\r\n    help|?       prints this message");
        putsU1("\r\n    status       prints version string");
        putsU1("\r\n    echo on|off  echo characters as they are received");
        putsU1("\r\n    a0..a5       reports analogue value");
        putsU1("\r\n    sample       begin sampling (waits for trigger event)");
        putsU1("\r\n    report       reports the complete set of sampled data");
        putsU1("\r\n    test spi ram");
        putsU1("\r\n");
        putsU1("\r\n    set pll <normal|slow>");
        putsU1("\r\n    set number_of_signals <value>");
        putsU1("\r\n    set total_samples <value>");
        putsU1("\r\n    set sample_period_us <value>");
        putsU1("\r\n    set post_trigger_samples <value>");
        putsU1("\r\n    set trigger_level <value>");
        putsU1("\r\n    set trigger_slope +|-");
        putsU1("\r\n    set trigger_hold_us <value>");
        putsU1("\r\n    set digipot <0..5> <value>");
        putsU1("\r\n    set laser <on|off>");
        putsU1("\r\n");
        putsU1("\r\nReady.");
        putsU1("\r\n");
        return;
    }
    if ( !strncmp(buf,"status",6) ) {
        putsU1("\r\nFirmware version: ");
        putsU1(VERSION_STRING);
        putsU1("\r\nSettings:");
        printf("\r\n    TEXT_BUFFER_SIZE= %u", TEXT_BUFFER_SIZE);
        printf("\r\n    echo_characters= %u", echo_characters);
        printf("\r\n    current FCY= %lu", getFCY());
        printf("\r\n    sample_period_us= %u (36 is lowest reliable)",
                sample_period_us);
        printf("\r\n    period_register_count= %u",
               compute_period_register_count(sample_period_us));
        printf("\r\n    number_of_signals= %u", number_of_signals);
        printf("\r\n    bytes_per_sample_set= %u", bytes_per_sample_set);
        printf("\r\n    storage= ");
        if ( storage == RAM_INT ) printf("INT"); else printf("EXT");
        printf("\r\n    total_samples= %lu", total_samples);
        printf("\r\n    record_period_in_seconds= %f", record_period_in_seconds());
        printf("\r\n    sample_id= %lu", sample_id);
        printf("\r\n    trigger_source= ");
        if ( trigger_source == TRIG_INT ) printf("INT"); else printf("EXT");
        printf("\r\n    post_trigger_samples= %lu", post_trigger_samples);
        printf("\r\n    post_trigger_percent= %lu", post_trigger_percent);
        printf("\r\n    trigger_signal= %u", trigger_signal);
        printf("\r\n    trigger_slope= ");
        if ( trigger_slope == POS ) printf("POS"); else printf("NEG");
        printf("\r\n    trigger_level= %u", trigger_level);
        printf("\r\n    trigger_hold_us= %u", trigger_hold_us);
        printf("\r\n    trigger_hold_samples= %u", trigger_hold_samples);
        printf("\r\n    digipot[0] (accel)= %u", digipot_settings[0]);
        printf("\r\n    digipot[1] (laser)= %u", digipot_settings[1]);
        printf("\r\n");
        putsU1("\r\nReady.");
        printf("\r\n");
        return;
    }
    if ( !strncmp(buf,"echo on",7) ) {
        echo_characters = 1;
        putsU1("\r\nEcho characters is on.");
        return;
    }
    if ( !strncmp(buf,"set pll slow",12) ) {
        setPLLslow();
        initUART1(DEFAULT_BAUD_RATE);
        printf("\r\ncurrent FCY= %lu", getFCY());
        return;
    }
    if ( !strncmp(buf,"set pll normal",14) ) {
        setPLLnormal();
        initUART1(DEFAULT_BAUD_RATE);
        printf("\r\ncurrent FCY= %lu", getFCY());
        return;
    }
    if ( !strncmp(buf,"sample",6) ) {
        putsU1("\r\nBegin sampling...");
        doSampling();
        putsU1("\r\nDone.");
        return;
    }
    if ( !strncmp(buf,"report",6) ) {
        reportSamples();
        return;
    }
    if ( buf[0] == 'a' || buf[0] == 'A' ) {
        which_ANx = (unsigned char) (buf[1] - '0');
        if ( which_ANx > 5 ) which_ANx = 5;
        avalue = readADC(anx[which_ANx]);
        switch (which_ANx) {
            case 0:
                printf("\r\nA%d (accel)=%d", which_ANx, avalue);
                break;
            case 1:
                printf("\r\nA%d (laser)=%d", which_ANx, avalue);
                break;
            default:
                printf("\r\nA%d=%d", which_ANx, avalue);
                break;
        }
        return;
    }
    if ( !strncmp(buf, "test spi ram", 12) ) {
        // Use the first byte of SPI RAM data as a seed
        unsigned char seed;
        readSRAMdata(SPI_RAM, 0x0000, &seed, 1);
        printf("\r\nseed = %u", seed);

        // Write pattern
        // 1 Mb = 131072 B = 4096 * 32 B buffers
        unsigned long i, j;
        unsigned char x = seed + 1;
        for (i = 0; i < 4096; i++) {
            for (j = 0; j < 32; j++) {
                pageBuf0[j] = x++;
            }
            writeSRAMdataFast(32 * i, pageBuf0, 32);
        }

        // Read pattern
        unsigned char good = 1;
        unsigned char y = seed + 1;
        for (i = 0; i < 4096; i++) {
            readSRAMdata(SPI_RAM, 32 * i, pageBuf0, 32);
            for (j = 0; j < 32; j++) {
                if (pageBuf0[j] != y++) {
                    printf("\r\nbad byte @ %lu", 32 * i + j);
                    good = 0;
                }
            }
        }

        // Print result
        if (good) {
            printf("\r\nOK");
        } else {
            printf("\r\nBAD");
        }
        return;
    }
    if ( !strncmp(buf,"set number_of_signals",21) ) {
        sscanf(&(buf[21]), "%u", &number_of_signals);
        // Want an even number because of the way we pack data.
        if ( number_of_signals % 2 ) number_of_signals += 1;
        number_of_signals = min(max(2,number_of_signals), MAX_ANALOG_SIGNALS);
        set_derived_parameters();
        printf("\r\nnumber_of_signals= %u", number_of_signals);
        printf("\r\ntotal_samples= %lu", total_samples);
        printf("\r\npost_trigger_samples= %lu", post_trigger_samples);
        return;
    }
    if ( !strncmp(buf,"set total_samples",17) ) {
        sscanf(&(buf[17]), "%lu", &total_samples);
        total_samples = max(128,total_samples);
        total_samples = min(total_samples, max_number_of_sample_sets());
        post_trigger_samples = min(post_trigger_samples,total_samples/2);
        printf("\r\ntotal_samples= %lu", total_samples);
        printf("\r\npost_trigger_samples= %lu", post_trigger_samples);
        printf("\r\nrecord_period= %f seconds", record_period_in_seconds());
        return;
    }
    if ( !strncmp(buf,"set sample_period_us",20) ) {
        sscanf(&(buf[20]), "%u", &sample_period_us);
        printf("\r\nsample_period_us= %u", sample_period_us);
        printf("\r\nrecord_period= %f seconds", record_period_in_seconds());
        return;
    }
    if ( !strncmp(buf,"set post_trigger_samples",24) ) {
        sscanf(&(buf[24]), "%lu", &post_trigger_samples);
        if ( post_trigger_samples > total_samples )
            post_trigger_samples = total_samples;
        printf("\r\npost_trigger_samples= %lu", post_trigger_samples);
        return;
    }
    if ( !strncmp(buf,"set trigger_level",17) ) {
        sscanf(&(buf[17]), "%u", &trigger_level);
        if ( trigger_level > 4095 ) trigger_level = 4095;
        printf("\r\ntrigger_level= %u", trigger_level);
        return;
    }
    if ( !strncmp(buf,"set trigger_slope",17) ) {
        if ( buf[18] == '-' ) {
            trigger_slope = NEG;
            printf("\r\ntrigger_slope= NEG");
        } else {
            // default is positive slope
            trigger_slope = POS;
            printf("\r\ntrigger_slope= POS");
        }
        return;
    }
    if ( !strncmp(buf,"set trigger_hold_us", 19) ) {
        sscanf(&(buf[19]), "%d", &trigger_hold_us);
        set_derived_parameters();
        printf("\r\ntrigger_hold_us= %u", trigger_hold_us);
        printf("\r\ntrigger_hold_samples= %u", trigger_hold_samples);
        return;
    }
    if (!strncmp(buf,"set digipot", 11)) {
        sscanf(&(buf[11]), "%d", &avalue);
        sscanf(&(buf[13]), "%d", &ivalue);
        if (avalue > 5) {
            avalue = 5;
        }
        if (ivalue > 255) {
            avalue = 255;
        }
        if (avalue > 1) {
            printf("\r\ndigipot %d not connected", avalue);
        } else {
            printf("\r\nset digipot %d to %d", avalue, ivalue);
            digipot_settings[avalue] = ivalue;
            writeDP(avalue, digipot_settings[avalue]);
        }
        return;
    }
    if ( !strncmp(buf,"set laser off",13) ) {
        LASER = 0;
        printf("\r\nlaser off");
        return;
    }
    if ( !strncmp(buf,"set laser on",12) ) {
        LASER = 1;
        printf("\r\nlaser on");
        return;
    }
    if ( !strncmp(buf,"set post_trigger_percent",24) ) {
        sscanf(&(buf[24]), "%lu", &post_trigger_percent);
        set_derived_parameters();
        printf("\r\npost_trigger_percent= %lu", post_trigger_percent);
        printf("\r\npost_trigger_samples= %lu", post_trigger_samples);
        return;
    }
    putsU1("\r\nUnknown command: ");
    putsU1(buf);
    return;
}

// ---------------------------------------------------------------------

int main(int argc, char** argv)
{
    initHardware();
    initPLL();
    initUART1(DEFAULT_BAUD_RATE);
    initADC();
    initSPIx();
    my_delay(255);

    // X3 Piston config
    number_of_signals = 2;

    // Trigger on accelerometer (channel 0)
    trigger_source = TRIG_INT;
    trigger_signal = 0;
    trigger_slope = POS;

    // Bench test
    post_trigger_percent = 80;
    trigger_level = 2000;
    trigger_hold_us = 3000;

    // Max sample rate
    sample_period_us = 36;
    set_derived_parameters();

    // Set gains
    enableDP();

    // Bench test
    digipot_settings[0] = 252;
    digipot_settings[1] = 175;
    writeDP(0, digipot_settings[0]); // Accelerometer (high is high)
    writeDP(1, digipot_settings[1]); // Photodiode (low is high)

    // Print welcome message
    printf("\r\nedaqs-x3-piston: %s", VERSION_STRING);
    printf("\r\nPeter Jacobs (modified by Thomas Hines)");
    printf("\r\nSchool of Engineering, Uni of Qld");
    printf("\r\n"); printResetCause();
    printf("\r\nPut SPI SRAM chip into sequential mode.");
    unsigned char register_value = setSRAMmode(SPI_RAM, SRAM_SEQUENTIAL_MODE);
    printf("\r\nmode register=0x%x", register_value);
    if (register_value == SRAM_SEQUENTIAL_MODE) printf(" OK"); else printf(" Bad");
    putsU1("\r\n");
    if ( echo_characters ) putsU1(PROMPT_STRING);
    while ( 1 ) {
        // LED_PIN ^= 1; // toggle LED
        getsnU1(text_buffer, TEXT_BUFFER_SIZE, echo_characters);
        interpretCommandString(text_buffer);
        if ( echo_characters ) putsU1(PROMPT_STRING);
    }
    return(EXIT_SUCCESS);
}
