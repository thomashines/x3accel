/*
 * File:   my_uart.c
 * Author: peterj
 *
 * Created on 20 February 2009.
 * Functions adapted from the Microchip datasheet information.
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
# else
#  error "not a valid MCU selection"
# endif
#endif

// Standard includes
#include <stdlib.h>

// eDAQS modules
#include "my_uart.h"
#include "osc.h"

// Initialize the serial port hardware, assuming
// low-speed range, no parity, 8 data bits, 1 stop bit.
// It is also assumed that appropriate connections to
// external pins are made elsewhere.
void initUART1(unsigned long baud_rate) {
    const unsigned int S = 16;
    U1BRG = getFCY()/S/baud_rate - 1;
    U1MODEbits.UARTEN = 1;
    U1STAbits.UTXEN = 1; // U1TX pin will be controlled by UART1
    U1STAbits.OERR = 0; // clear error flags
    U1STAbits.FERR = 0;
    U1STAbits.PERR = 0;
    return;
}

// Put a character out through the serial port.
char putcU1(char c) {
    while ( U1STAbits.UTXBF ) /* wait while buffer is full */ ;
    U1TXREG = c;
    return c;
}

// Get the next character from the serial port.
// Note that this call is blocking.  It will wait for the character.
char getcU1(void) {
    // Clear any errors encountered, UART is not critical
    if ( U1STAbits.FERR ) U1STAbits.FERR = 0 /* clear and continue */ ;
    if ( U1STAbits.OERR ) U1STAbits.OERR = 0 /* clear and continue */ ;
    while ( !U1STAbits.URXDA ) /* wait for a new character */ ;
    return U1RXREG;
}

// Put the string out through the serial port.
unsigned int putsU1(char *s) {
    unsigned int count = 0;
    while ( *s != '\0' ) {
        putcU1( *s );
        ++s;
        ++count;
    }
    return count;
}

// Get a text string from the serial port up to a maximum of n characters
// (including the terminating null character)
// or up until a carriage-return '\r' is encountered.
// A null-terminated string is returned.
char * getsnU1(char *buf, unsigned int n, unsigned char with_echo) {
    char *p = buf; // We are going to add characters at p.
    int length_remaining = n - 1;
    while ( length_remaining > 0 ) {
        char c = getcU1();
        if ( c == '\r' ) break; // Don't include the return character.
        if ( c == '\n' ) continue; // Discard new line characters.
        if ( ( c == BS ) && ( p > buf ) ) {
            // Remove the previous character.
            p--; length_remaining++;
            if ( with_echo ) {
                // Go back one character
                putsU1("\033[D \033[D");
            }
            continue;
        }
        if ( c == BS ) {
            // Discard excess backspace characters.
            continue;
        }
        // Add the character to the buffer.
        *p = c;
        p++; length_remaining--;
        if ( with_echo ) { putcU1(c); }
    }
    *p = '\0';
    return buf;
}

// Service function for stdio library.
// For the moment, put everything through UART1.
int write(int handle, void *buffer, unsigned int len) {
    int i;
    char *cp;

    switch ( handle ) {
        default:
        case 0:  // stdin
        case 1:  // stdout
        case 2:  // stderr
            for ( i = len; i; --i ) {
                cp = (char *)buffer++;
                putcU1( *cp );
            }
            break;
    } // end switch
    return len;
}

// Derived from Figure 8.18 of Reese et al. (page 279)
void printResetCause(void) {
    if ( RCONbits.SLEEP ) {
        putsU1("Device has been in sleep mode.");
        RCONbits.SLEEP = 0;
    }
    if ( RCONbits.POR ) {
        putsU1("Power-on reset.");
        RCONbits.POR = 0;
        RCONbits.BOR = 0;
    } else {
        // non-POR causes
        if ( RCONbits.SWR ) {
            putsU1("Software reset.");
            RCONbits.SWR = 0;
        }
        if ( RCONbits.WDTO ) {
            putsU1("Watchdog timeout reset.");
            RCONbits.WDTO = 0;
        }
        if ( RCONbits.EXTR ) {
            putsU1("Master-clear-pin reset.");
            RCONbits.EXTR = 0;
        }
        if ( RCONbits.BOR ) {
            putsU1("Brown-out reset.");
            RCONbits.BOR = 0;
        }
        if ( RCONbits.TRAPR ) {
            putsU1("Trap conflict reset.");
            RCONbits.TRAPR = 0;
        }
        if ( RCONbits.IOPUWR ) {
            putsU1("Illegal condition device reset.");
            RCONbits.IOPUWR = 0;
        }
        if ( RCONbits.CM ) {
            putsU1("Configuration mismatch reset.");
            RCONbits.CM = 0;
        }
        return;
    }
    return;
}
