/*
 * File:   osc.c
 * Author: peterj
 *
 * Created on 8 February 2014.
 * Clock setting functions.
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
# else
#  error "not a valid MCU selection"
# endif
#endif

// eDAQS modules
#include "osc.h"

static char PLL_multiplier = 2;

void initPLL() {
    // Configure PLL to multiply FIN by 4
    // This gives FOSC=29.49MHz from FIN=7.3728MHz
    PLLFBD = 30;
    CLKDIVbits.PLLPOST = 1;
    CLKDIVbits.PLLPRE = 0;
    PLL_multiplier = 4;
    // New clock source will be primary oscillator with PLL.
    __builtin_write_OSCCONH(0b011);
    // Now, we want to make the switch without upsetting other settings.
    __builtin_write_OSCCONL(OSCCON | 0x01);
    // Wait for Clock switch to occur
    while (OSCCONbits.COSC != 0b011);
    // Wait for PLL to lock
    while(OSCCONbits.LOCK!=1);

    return;
}

void setPLLnormal() {
    CLKDIVbits.PLLPOST = 1;
    PLL_multiplier = 4;
}

void setPLLslow() {
    CLKDIVbits.PLLPOST = 3;
    PLL_multiplier = 2;
}

unsigned long getFCY() {
    return FCRYSTAL / 2 * PLL_multiplier;
}
