/*
 * File:   spi_chips.c
 * Author: peterj
 *
 * Created on 1 February 2014.
 * Service functions for the 23LC1024 SRAM chip in SPI mode.
 * Adapted from the daqs-drb project.
 */


#include "xc.h"

// Include hardware configuration
#ifdef __dsPIC33EP256GP502__
# include <p33Exxxx.h>
# define SPIxCON1bits SPI2CON1bits
# define SPIxCON2bits SPI2CON2bits
# define SPIxSTATbits SPI2STATbits
# define SPIxIF SPI2IF
# define SPIxBUF SPI2BUF
# define IFSxbits IFS2bits
#else
# ifdef __dsPIC33FJ128GP802__
#  include <p33Fxxxx.h>
#  define SPIxCON1bits SPI1CON1bits
#  define SPIxCON2bits SPI1CON2bits
#  define SPIxSTATbits SPI1STATbits
#  define SPIxIF SPI1IF
#  define SPIxBUF SPI1BUF
#  define IFSxbits IFS0bits
# else
#  error "not a valid MCU selection"
# endif
#endif

// eDAQS modules
#include "osc.h"
#include "spi_chips.h"

#define CS_SRAM (LATBbits.LATB13)
#define CS_DP   (LATBbits.LATB7)

void selectSPIchip(enum spi_chips_t whichChip) {
    switch ( whichChip ) {
        case SPI_RAM:
            // Active-low selects chip.
            CS_SRAM = 0;
            // Must not have more than one selected.
            CS_DP = 1;
            break;
        case SPI_DP:
            CS_DP = 0;
            CS_SRAM = 1;
            break;
        default:
            CS_SRAM = 1;
            CS_DP = 1;
    }
	return;
}

void releaseSPIchip(enum spi_chips_t whichChip) {
    switch ( whichChip ) {
        case SPI_RAM:
            CS_SRAM = 1;
            break;
        case SPI_DP:
            CS_DP = 1;
            break;
        default:
            CS_SRAM = 1;
            CS_DP = 1;
    }
	return;
}

void initSPIx(void) {
    // Assume that the IO-pins have been already assigned
    // back in initHardware().
    SPIxCON1bits.MSTEN = 1; // Master mode
    SPIxSTATbits.SPIROV = 0; // clear receive overflow flag
    SPIxCON1bits.MODE16 = 0; // 8 bit word size
    SPIxCON2bits.SPIBEN = 1; // Enhanced buffer enabled

    // Primary * Secondary prescale to give clock freq of about 7.4MHz
    SPIxCON1bits.PPRE = 0b11; // 1:1 primary prescale
    SPIxCON1bits.SPRE = 0b110; // 2:1 secondary prescale
    //
    // The 23K256 data sheet shows the SPI clock with positive polarity
    // having the clock pulses starting with a rising edge.
    // It is also stated that the SRAM chip puts its data out on the
    // falling clock edge (which is half-way through a cycle).
    // Hence we should wait half an SPI clock cycle before sampling
    // the incoming data.
    SPIxCON1bits.SMP = 1; // input data sampled at end of data output time
    SPIxCON1bits.CKE = 1; // serial output data changes on transition
                          // from active to idle clock state
    SPIxCON1bits.CKP = 0; // Clock polarity, idle low, active high
    // Accept power-on defaults for other bits.
    IFSxbits.SPIxIF = 0; // clear interrupt flag
    SPIxSTATbits.SPIEN = 1; // turn it on
    return;
}

// 8-bit SPI SRAM commands
#define SRAM_READ 0b00000011
#define SRAM_WRITE 0b00000010
// RDSR: Read Status Register
#define SRAM_RDSR 0b00000101
// WRSR: Write Status Register
#define SRAM_WRSR 0b00000001

// Digital potentiometer rules
#define DP_VW0    0x00
#define DP_VW1    0x10
#define DP_TCON   0x40
#define DP_STATUS 0x50

void putcSPIx(unsigned char cout) {
    // Transmit
    // printf("\r\n tx");
    SPIxBUF = cout;
    // Wait for transmission to finish
    // printf("\r\n wait tx");
    while ( SPIxSTATbits.SPIBEC ) ;
}

unsigned char getcSPIx() {
    // Wait to receive something
    // printf("\r\n wait rx");
    while ( SPIxSTATbits.SRXMPT ) ;
    // Return the received message
    // printf("\r\n rx");
    return SPIxBUF;
}

unsigned char putcgetcSPIx(unsigned char cout) {
    putcSPIx(cout);
    return getcSPIx();
}

void clearRx() {
    unsigned char dummy;
    while ( !SPIxSTATbits.SRXMPT ) {
        dummy = SPIxBUF;
    }
}

unsigned char setSRAMmode(enum spi_chips_t which_chip, unsigned char mode) {
    selectSPIchip(which_chip);
    putcSPIx(SRAM_WRSR);
    putcSPIx(mode);
    releaseSPIchip(which_chip);
    Nop();
    selectSPIchip(which_chip);
    putcSPIx(SRAM_RDSR);
    clearRx();
    unsigned char register_value = putcgetcSPIx(0xff);
    releaseSPIchip(which_chip);
    return register_value;
}

unsigned char writeSRAMdata(enum spi_chips_t which_chip, unsigned long addr,
                            unsigned char *bufout, unsigned int n) {
    selectSPIchip(which_chip);
    putcSPIx(SRAM_WRITE);
    putcSPIx((unsigned char)(addr >> 16));
    putcSPIx((unsigned char)(addr >> 8));
    putcSPIx((unsigned char)addr);
    int i;
    for (i = 0; i < n; i++) {
        putcSPIx(*bufout++);
        clearRx();
    }
    releaseSPIchip(which_chip);
    return 0;
}

unsigned char writeSRAMdataFast(unsigned long addr,
                                unsigned char *bufout, unsigned int n) {
    // Select SRAM
    CS_SRAM = 0;
    // Transmit write command
    SPIxBUF = SRAM_WRITE;
    // Transmit address
    SPIxBUF = (addr >> 16);
    SPIxBUF = (addr >> 8);
    SPIxBUF = addr;
    // Transmit data
    int i;
    unsigned char dummy;
    for (i = 0; i < n; i++) {
        // Wait for a spot in the transmission queue
        while ( SPIxSTATbits.SPITBF ) ;
        // Transmit the byte
        SPIxBUF = *bufout++;
        // Clear the receiving buffer, not sure why this is required
        while ( !SPIxSTATbits.SRXMPT ) {
            dummy = SPIxBUF;
        }
    }
    // Wait for transmission to finish
    while ( SPIxSTATbits.SPIBEC ) ;
    // Deselect SRAM
    CS_SRAM = 1;
    return 0;
}

unsigned char readSRAMdata(enum spi_chips_t which_chip, unsigned long addr,
                           unsigned char *bufin, unsigned int n) {
    selectSPIchip(which_chip);
    // Transmit read command
    putcSPIx(SRAM_READ);
    // Transmit address
    putcSPIx((unsigned char)(addr >> 16));
    putcSPIx((unsigned char)(addr >> 8));
    putcSPIx((unsigned char)addr);
    // Clear receving buffer
    clearRx();
    // Get data
    int i;
    for (i = 0; i < n; i++) {
        *bufin++ = putcgetcSPIx(0xff);
    }
    releaseSPIchip(which_chip);
    return 0;
}

unsigned char enableDP(void) {
    unsigned char dummy;

    // Select the digipot
    selectSPIchip(SPI_DP);

    // Send the digipot TCON address
    dummy = putcgetcSPIx(DP_TCON);

    // Send the enable byte resistance
    dummy = putcgetcSPIx(0xFF);

    // Deselect the digipot
    releaseSPIchip(SPI_DP);

    return dummy;
}

unsigned char writeDP(char which_dp, char res) {
    enum spi_chips_t dp_chip;
    unsigned char dp_addr;
    unsigned char dummy;

    switch (which_dp) {
        case 0:
            dp_chip = SPI_DP;
            dp_addr = DP_VW0;
            break;
        case 1:
            dp_chip = SPI_DP;
            dp_addr = DP_VW1;
            break;
        default:
            dp_chip = SPI_DP;
            dp_addr = DP_VW0;
            break;
    }

    // Select the digipot
    selectSPIchip(dp_chip);

    // Send the digipot wiper address
    dummy = putcgetcSPIx(dp_addr);

    // Send the digipot resistance
    dummy = putcgetcSPIx(res);

    // Deselect the digipot
    releaseSPIchip(dp_chip);

    return dummy;
}
