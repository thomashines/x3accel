# pll-settings.py
FIN = 7.3728
PLLPRE = 0.
PLLDIV = 30.
PLLPOST = 1.

N1 = PLLPRE+2
print "N1=", N1
N2 = 2 * (PLLPOST + 1)
print "N2=", N2
M = PLLDIV + 2
print "M=", M

FVCO = FIN * M/N1
print "FVCO=", FVCO
PLL_multiply = M/(N1*N2)
print "PLL_multiply=", PLL_multiply
FOSC = FIN * PLL_multiply
print "FOSC=", FOSC
